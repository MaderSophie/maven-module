# Maven Module

In diesem Repository finden wir eine simple Anwendung mit der sich Aufgaben / Todos verwalten lassen.
Viel Funktionalität wurde noch nicht umgesetzt, allerdings geht es hier mehr um die Struktur der 
Anwendung als um die konkreten Features.

## Module

Die Anwendung ist in drei Module geteilt:

1. `todos`: Im todos-Modul befindet sich der Kern der Anwendung. In diesem Modul sind jene Klassen abgelegt die das Verwalten von Aufgaben implementieren.
2. `desktop-ui`: Im Modul desktop-ui befindet sich Code für die grafische Benutzerschnittstelle. Dieses Modul ist vom `todos` Modul abhängig.
3. `console-ui`: Das console-ui Modul implementiert eine zweite Variante der Benutzerschnittstelle, nämlich die Konsole. Dieses Modul ist ebenfalls von `todos` abhängig.

## Anwendung ausführen

Nach dem Klonen des Repositories muss zuerst im Repository-Verzeichnis der Befehl 

    mvn install

ausgeführt werden. Wenn das erfolgreich durchläuft, lassen sich die `main()`-Methoden der Klassen

- `ConsoleUIMain` im Modul console-ui
- `DesktopUIMain` im Modul desktop-ui

ausführen.