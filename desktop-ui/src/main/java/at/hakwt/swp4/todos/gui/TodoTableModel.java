package at.hakwt.swp4.todos.gui;

import at.hakwt.swp4.todos.model.Todo;

import javax.swing.table.AbstractTableModel;
import java.util.List;

public class TodoTableModel extends AbstractTableModel {

    private final List<Todo> todos;

    public TodoTableModel(List<Todo> todos) {
        this.todos = todos;
    }

    private static final String[] COLUMNS = {"finished?", "user", "Todo"};

    @Override
    public int getRowCount() {
        return this.todos.size();
    }

    @Override
    public int getColumnCount() {
        return COLUMNS.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Todo todo = todos.get(rowIndex);
        return switch (columnIndex) {
            case 0 -> todo.isFinished();
            case 1 -> todo.getAssignedUser().getName();
            case 2 -> todo.getDescription();
            default -> "";
        };
    }

    @Override
    public String getColumnName(int column) {
        return COLUMNS[column];
    }

}
